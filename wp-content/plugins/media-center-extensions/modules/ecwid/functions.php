<?php

if( ! function_exists( 'mc_ext_ecwid_api' ) ) {
	function mc_ext_ecwid_api() {
		return new Ecwid_Api_V3();
	}
}

if( ! function_exists( 'mc_ext_ecwid_product_html' ) ) {
	function mc_ext_ecwid_product_html( $product = array() ) {

		if( ! empty( $product ) ) {
			if( is_array( $product ) ) {
				$product_id = $product['id'];
				$product_url = isset( $product['url'] ) ? $product['url'] : '';
				$product_name = isset( $product['name'] ) ? $product['name'] : '';
				$product_thumbnail_url = isset( $product['thumbnailUrl'] ) ? $product['thumbnailUrl'] : '';
				$product_favorite_count = isset( $product['favorites']['displayedCount'] ) ? $product['favorites']['displayedCount'] : 0;
				$product_price = isset( $product['price'] ) ? $product['price'] : 0;
				$product_price_formatted = isset( $product['defaultDisplayedPriceFormatted'] ) ? $product['defaultDisplayedPriceFormatted'] : 0;
			} else {
				$product_id = $product->id;
				$product_url = isset( $product->url ) ? $product->url : '';
				$product_name = isset( $product->name ) ? $product->name : '';
				$product_thumbnail_url = isset( $product->thumbnailUrl ) ? $product->thumbnailUrl : '';
				$product_favorite_count = isset( $product->favorites->displayedCount ) ? $product->favorites->displayedCount : 0;
				$product_price = isset( $product->price ) ? $product->price : 0;
				$product_price_formatted = isset( $product->defaultDisplayedPriceFormatted ) ? $product->defaultDisplayedPriceFormatted : '';
			}

			ob_start();
			?>
			<div class="product">
				<div class="product-inner">
					<a href="<?php echo ecwid_get_product_url( array( 'id' => $product_id, 'url' => $product_url ) ); ?>">
						<div class="ecwid-favorite">
							<span><?php echo $product_favorite_count; ?></span>
						</div>

						<div class="product-thumbnail-wrapper">
							<?php if( ! empty( $product_thumbnail_url ) ) : ?>
								<img class="attachment-shop_catalog wp-post-image" width="246" height="186" src="<?php echo $product_thumbnail_url; ?>" alt="">
							<?php endif; ?>
						</div>

						<div class="title-area">
							<h3><?php echo $product_name; ?></h3>
						</div>

						<span class="price">
							<span class="mc-price-wrapper">
								<span class="amount"><?php echo $product_price_formatted; ?></span>
							</span>
						</span>
					</a>
				</div>
			</div>
			<?php
			$html = ob_get_clean();
			return $html;
		}

		return;
	}
}
