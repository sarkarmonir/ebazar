<?php

if ( !function_exists( 'shortcode_mc_ecwid_home_tabs' ) ):

function shortcode_mc_ecwid_home_tabs( $atts, $content = null ) {

	extract(shortcode_atts(array(
		'title_tab_1'		=> '',
		'content_sc_tab_1'	=> '',
		'title_tab_2'		=> '',
		'content_sc_tab_2'	=> '',
		'title_tab_3'		=> '',
		'content_sc_tab_3'	=> '',
	), $atts));

	if( empty( $title_tab_1 ) && empty( $title_tab_2 ) && empty( $title_tab_3 ) ) {
		return;
	}

	$limit = 4;
	$sorts = array(
		'ADDED_TIME_DESC',
		'ADDED_TIME_ASC',
		'NAME_ASC',
		'NAME_DESC',
		'PRICE_ASC',
		'PRICE_DESC',
		'UPDATED_TIME_ASC',
		'UPDATED_TIME_DESC'
	);
	$ecwid_api = mc_ext_ecwid_api();

	$products_tab_contents = array();
	if( ! empty( $content_sc_tab_1 ) ) {
		$found = $ecwid_api->search_products(array(
			'category' => $content_sc_tab_1,
			'limit' => $limit
		));
		$products_tab_contents[] = $found->items;
	} else {
		$found = $ecwid_api->search_products(array(
			'sortBy' => $sorts[rand(0, 7)],
			'limit' => $limit
		));
		$products_tab_contents[] = $found->items;
	}
	if( ! empty( $content_sc_tab_2 ) ) {
		$found = $ecwid_api->search_products(array(
			'category' => $content_sc_tab_1,
			'limit' => $limit
		));
		$products_tab_contents[] = $found->items;
	} else {
		$found = $ecwid_api->search_products(array(
			'sortBy' => $sorts[rand(0, 7)],
			'limit' => $limit
		));
		$products_tab_contents[] = $found->items;
	}
	if( ! empty( $content_sc_tab_3 ) ) {
		$found = $ecwid_api->search_products(array(
			'category' => $content_sc_tab_1,
			'limit' => $limit
		));
		$products_tab_contents[] = $found->items;
	} else {
		$found = $ecwid_api->search_products(array(
			'sortBy' => $sorts[rand(0, 7)],
			'limit' => $limit
		));
		$products_tab_contents[] = $found->items;
	}

	$tab_content = array();
	foreach ( $products_tab_contents as $products_tab_content ) {
		$product_content = '';
		$product_content .='<div class="products columns-4">';
		foreach ( $products_tab_content as $product ) {
			$product_content .= mc_ext_ecwid_product_html( $product );
		}
		$product_content .= '</div>';
		$tab_content[] = $product_content;
	}

	ob_start();
	?>
	<div class="inner-top-xs inner-bottom-sm home-page-tabs ecwid-home-page-tabs">
		<div class="tab-holder">
			<ul class="nav nav-tabs">
				<?php if( !empty( $title_tab_1 ) ) : ?>
				<li class="active"><a data-toggle="tab" href="#home-page-tab-1"><?php echo $title_tab_1; ?></a></li>
				<?php endif ; ?>
				<?php if( !empty( $title_tab_2 ) ) : ?>
				<li><a data-toggle="tab" href="#home-page-tab-2"><?php echo $title_tab_2; ?></a></li>
				<?php endif; ?>
				<?php if( !empty( $title_tab_3 ) ) : ?>
				<li><a data-toggle="tab" href="#home-page-tab-3"><?php echo $title_tab_3; ?></a></li>
				<?php endif; ?>
			</ul><!-- /.nav-tabs -->
			<div class="tab-content">
				<?php if( !empty( $tab_content[0] ) ) : ?>
				<div id="home-page-tab-1" class="tab-pane active"><?php echo $tab_content[0];?></div><!-- /.tab-pane -->
				<?php endif; ?>
				<?php if( !empty( $tab_content[1] ) ) : ?>
				<div id="home-page-tab-2" class="tab-pane"><?php echo $tab_content[1];?></div><!-- /.tab-pane -->
				<?php endif; ?>
				<?php if( !empty( $tab_content[2] ) ) : ?>
				<div id="home-page-tab-3" class="tab-pane"><?php echo $tab_content[2];?></div><!-- /.tab-pane -->
				<?php endif; ?>
			</div><!-- /.tab-content -->
		</div><!-- /.tab-holder -->
	</div><!-- /.home-page-tabs -->
	<?php

    $home_tabs = ob_get_clean();

    return $home_tabs;
}

add_shortcode( 'mc_ecwid_home_tabs' , 'shortcode_mc_ecwid_home_tabs' );
endif;