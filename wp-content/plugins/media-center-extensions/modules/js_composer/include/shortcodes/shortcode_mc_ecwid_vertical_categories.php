<?php

if ( !function_exists( 'shortcode_mc_ecwid_vertical_categories' ) ):

function shortcode_mc_ecwid_vertical_categories( $atts, $content = null ){

	extract(shortcode_atts(array(
		'title'	=> ''
	), $atts));

	$widget_class = 'Ecwid_Widget_Vertical_Categories_List';

	$output = '';
	if( class_exists( 'Ecwid_Api_V3' ) && Ecwid_Api_V3::is_available() && class_exists( $widget_class ) ) {
		ob_start();
		the_widget( $widget_class, array( 'title' => $title ) );
		$output = ob_get_clean();
	}

	return $output;

}

add_shortcode( 'mc_ecwid_vertical_categories' , 'shortcode_mc_ecwid_vertical_categories' );
endif;