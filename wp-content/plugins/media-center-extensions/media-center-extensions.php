<?php
/*
Plugin Name: Media Center Extensions
Plugin URI: http://demo.transvelo.com/media-center-wp/
Description: Extensions for Media Center Wordpress Theme. Supplied as a separate plugin so that the customer does not find empty shortcodes on changing the theme.
Version: 2.7.11
Author: Transvelo
Author URI: http://transvelo.com/
*/

// don't load directly
if ( ! defined( 'ABSPATH' ) ) die( '-1' );

global $mc_plugin_dir;

$mc_plugin_dir = plugin_dir_path( __FILE__ );

/**
 * Check if ECWID is activated
 */
if( ! function_exists( 'is_ecwid_activated' ) ) {
	function is_ecwid_activated() {
		return function_exists( 'get_ecwid_store_id' ) ? true : false;
	}
}

/**
 * Check if WooCommerce is activated
 */
if ( ! function_exists( 'is_woocommerce_active' ) ) {
	function is_woocommerce_active() {

		$active_plugins = (array) get_option( 'active_plugins', array() );

		if ( is_multisite() ) {
			$active_plugins = array_merge( $active_plugins, get_site_option( 'active_sitewide_plugins', array() ) );
		}

		return in_array( 'woocommerce/woocommerce.php', $active_plugins ) || array_key_exists( 'woocommerce/woocommerce.php', $active_plugins );
	}
}

//Load Modules

#-----------------------------------------------------------------
# Theme Shortcodes
#-----------------------------------------------------------------

require_once 'modules/theme-shortcodes/theme-shortcodes.php';

#-----------------------------------------------------------------
# Static Block Post Type for Megamenu Item
#-----------------------------------------------------------------

require_once 'modules/post-type-static-block/post-type-static-block.php';

if ( is_woocommerce_active() ) {
	
	#-----------------------------------------------------------------
	# Product Taxonomies
	#-----------------------------------------------------------------

	require_once 'modules/product-taxonomies/class-mc-product-taxonomies.php';

	#-----------------------------------------------------------------
	# Brands Short Code
	#-----------------------------------------------------------------

	require_once 'modules/theme-shortcodes/class-mc-brand-shortcodes.php';

}

#-----------------------------------------------------------------
# Visual Composer Extensions
#-----------------------------------------------------------------

if ( defined( 'WPB_VC_VERSION' ) ) {
	require_once 'modules/js_composer/js_composer.php';
}

#-----------------------------------------------------------------
# Ecwid
#-----------------------------------------------------------------

if( function_exists( 'is_ecwid_activated' ) && is_ecwid_activated() ) {
	require_once 'modules/ecwid/functions.php';
}

add_action( 'plugins_loaded', 'mediacenter_extensions_load_textdomain' );
/**
 * Load plugin textdomain.
 *
 * @since 1.2.2
 */
function mediacenter_extensions_load_textdomain() {
	load_plugin_textdomain( 'mc-ext', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' ); 
}