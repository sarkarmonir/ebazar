<?php

/**
 * Display Cart Page Title
 *
 * @return void
 */
if( ! function_exists( 'mc_display_cart_page_title' ) ) {
	function mc_display_cart_page_title() {
		?>
		<div class="section section-page-title inner-xs">
			<div class="page-header">
				<h2 class="page-title"><?php _e( 'Shopping Cart Summary', 'mediacenter' ); ?></h2>
			</div>
		</div>
		<?php
	}
}

if( ! function_exists( 'mc_my_account_page_title' ) ) {
	function mc_my_account_page_title() {
		$page_title = get_the_title();
		$dashboard_title = esc_html__( 'Dashboard', 'mediacenter' );
		?>
		<div class="woocommerce-MyAccount-navigation-title">
			<h4><?php echo apply_filters( 'mc_my_account_navigation_title', $page_title ); ?></h4>
		</div>
		<div class="woocommerce-MyAccount-content-title">
			<h2><?php echo apply_filters( 'mc_my_account_content_title', wc_page_endpoint_title( $dashboard_title ) ); ?></h2>
		</div>
		<?php
	}
}

/**
 * Displays Proceed to Checkout Action
 *
 * @return void
 */
if( ! function_exists( 'mc_proceed_to_checkout' ) ) {
	function mc_proceed_to_checkout() {
		?>
		<div class="wc-proceed-to-checkout">
			<?php do_action( 'woocommerce_proceed_to_checkout' ); ?>
		</div>
		<?php
	}
}

/**
 * Displays WooCommerce Checkout Shipping Fields title
 *
 * @return void
 */
if( ! function_exists( 'mc_checkout_shipping_fields_title' ) ) {
	function mc_checkout_shipping_fields_title() {
		if ( WC()->cart->needs_shipping_address() === true ) :
		?>
			<h3 class="shipping-details-title"><?php _e( 'Shipping Details', 'mediacenter' );?></h3>
		<?php
		endif;
	}
}

/**
 * Displays Signup Benefits after registration form
 *
 * @return void
 */
if( ! function_exists( 'mc_list_signup_benefits' ) ) {
	function mc_list_signup_benefits() {
		$benefits = apply_filters( 'mc_signup_benefits', array(
			__( 'Speed your way through the checkout', 'mediacenter' ),
			__( 'Track your orders easily', 'mediacenter' ),
			__( 'Keep a record of all your purchases', 'mediacenter' )
		) );
	?>
	<section class="section why-register inner-top-xs">
		<h2><?php echo __( 'Sign up today and you\'ll be able to :', 'mediacenter' ); ?></h2>
		<ul class="list-unstyled list-benefits">
			<?php foreach( $benefits as $benefit ) : ?>
			<li><i class="fa fa-check primary-color"></i> <?php echo $benefit; ?></li>
			<?php endforeach; ?>
		</ul>
	</section><!-- /.why-register -->
	<?php
	}
}

/**
 * Displays goto my account link
 *
 * @return void
 */
if( ! function_exists( 'mc_display_goto_my_account_link' ) ) {
	function mc_display_goto_my_account_link() {
		?>
		<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" class="goto-my-account-link"><i class="fa fa-arrow-left"></i>&nbsp;<?php _e( 'go to My Account page', 'mediacenter' ); ?></i>
		<?php
	}
}

/**
 * Wrapper start for customer login form
 *
 * @return void
 */
if( ! function_exists( 'mc_wrap_login_form_start' ) ) {
	function mc_wrap_login_form_start() {
		?>
		<div class="wrap-customer-login-form">
		<?php
	}
}

/**
 * Wrapper end for customer login form
 *
 * @return void
 */
if( ! function_exists( 'mc_wrap_login_form_end' ) ) {
	function mc_wrap_login_form_end() {
		?>
		</div><!-- /.wrap-customer-login-form -->
		<?php
	}
}

/**
 * Displays the mini cart of header
 *
 * @return void
 */
if( ! function_exists( 'mc_mini_cart' ) ) {
	function mc_mini_cart() {
		mc_get_template( 'shop/mc-mini-cart.php' );
	}
}

/**
 * Start of Single Product Summary Wrapper
 *
 * @return void
 */
if( ! function_exists( 'mc_product_summary_wrapper_start' ) ) {
	function mc_product_summary_wrapper_start() {
		?>
		<div class="images-and-summary-wrapper">
			<div class="images-and-summary">
		<?php
	}
}

/**
 * End of Single Product Summary Wrapper
 *
 * @return void
 */
if( ! function_exists( 'mc_product_summary_wrapper_end' ) ) {
	function mc_product_summary_wrapper_end() {
		?>
			</div><!-- /.images-and-summary -->
		</div><!-- /.images-and-summary-wrapper -->
		<?php
	}
}

/**
 * Start of wrapper for images
 *
 * @return void
 */
if( ! function_exists( 'mc_product_images_wrapper_start' ) ) {
	function mc_product_images_wrapper_start() {
		?>
		<div class="product-images">
		<?php
	}
}

/**
 * End of wrapper for images
 *
 * @return void
 */
if( ! function_exists( 'mc_product_images_wrapper_end' ) ) {
	function mc_product_images_wrapper_end() {
		?>
		</div><!-- /.product-images -->
		<?php
	}
}

/**
 * Displays WooCommerce Single Product Title
 *
 * @return void
 */
if( ! function_exists( 'mc_template_single_title' ) ) {
	function mc_template_single_title() {
		?>
		<div class="single-product-title">
			<?php woocommerce_template_single_title(); ?>
			<?php mc_template_single_brand(); ?>
		</div>
		<?php
	}
}

/**
 * Displays Product Brands
 *
 * @return void
 */
if( ! function_exists( 'mc_template_single_brand' ) ) {
	function mc_template_single_brand() {
		global $product;

		$brands = mc_get_product_brand( $product );
		$on_brand = join( ', ', $brands );
		?>
		<div class="product-brand"><?php echo $on_brand; ?></div>
		<?php
	}
}

/**
 * Output the start of page wrapper
 */
if( ! function_exists( 'mc_output_content_wrapper' ) ) {
	function mc_output_content_wrapper() {
		?>
		<div id="content" class="site-content container">
			<div class="row">
		<?php
	}
}

if( ! function_exists( 'mc_output_primary_wrapper' ) ) {
	function mc_output_primary_wrapper() {
		?>
		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">
		<?php
	}
}

if( ! function_exists( 'mc_output_primary_wrapper_end' ) ) {
	function mc_output_primary_wrapper_end() {
		?>
			</main><!-- /.site-main -->
		</div><!-- /#content -->
		<?php
	}
}

if( ! function_exists( 'mc_output_secondary_wrapper' ) ) {
	function mc_output_secondary_wrapper() {
		?>
		<div id="sidebar" class="sidebar">
			<div id="secondary" class="secondary">
		<?php
	}
}

if( ! function_exists( 'mc_output_secondary_wrapper_end' ) ) {
	function mc_output_secondary_wrapper_end() {
		?>
			</div><!-- /.secondary -->
		</div><!-- /.sidebar -->
		<?php
	}
}

/**
 * Output the end of page wrapper
 */
if( ! function_exists( 'mc_output_content_wrapper_end' ) ) {
	function mc_output_content_wrapper_end() {
		?>
			</div><!-- /.row -->
		</div><!-- /.site-content -->
		<?php
	}
}

if( ! function_exists( 'mc_loop_product_excerpt' ) ) {
	/**
	 * Displays product item excerpt
	 *
	 * @since 2.0.0
	 * @return void
	 */
	function mc_loop_product_excerpt() {
		?>
		<div class="excerpt">
			<?php
				$post_excerpt = wp_strip_all_tags( get_the_excerpt() );

				$new_excerpt_length = absint( apply_filters( 'mc_excerpt_list_view_length', 160 ) );
				$new_excerpt = '';

				if( strlen( $post_excerpt ) < $new_excerpt_length ){
					$new_excerpt = $post_excerpt;
				}else{
					$new_excerpt = substr( $post_excerpt, 0 , $new_excerpt_length ) . '...';
				}

				echo apply_filters( 'mc_excerpt_list_view', $new_excerpt );
			?>
		</div>
		<?php
	}
}

if( ! function_exists( 'mc_output_list_view_footer_start' ) ) {
	/**
	 * Outputs List View footer div start
	 *
	 * @return void
	 */
	function mc_output_list_view_footer_start() {
		?>
		<div class="list-view-footer">
		<?php
	}
}

if( ! function_exists( 'mc_output_list_view_footer_end' ) ) {
	/**
	 * Outputs List View footer div end
	 *
	 * @return void
	 */
	function mc_output_list_view_footer_end() {
		?>
		</div><!-- /.list-view-footer-end -->
		<?php
	}
}

if( ! function_exists( 'mc_shop_view_switcher' ) ) {
	function mc_shop_view_switcher() {
			$active_view = apply_filters( 'mc_default_view_switcher_view', 'grid' );
		?>
		<ul class="shop-view-switcher">
			<li <?php if( $active_view == 'grid' ) : ?>class="active"<?php endif; ?>>
				<a href="#grid-view" data-toggle="tab">
					<i class="fa fa-th-large"></i>
					<?php _e( 'Grid', 'mediacenter' ); ?>
				</a>
			</li>
			<li <?php if( $active_view == 'list' ) : ?>class="active"<?php endif; ?>>
				<a href="#list-view" data-toggle="tab">
					<i class="fa fa-th-list"></i>
					<?php _e( 'List', 'mediacenter' ); ?>
				</a>
			</li>
		</ul>
		<?php
	}
}

if( ! function_exists( 'woocommerce_product_loop_start' ) ) {
	function woocommerce_product_loop_start( $echo = true ) {

		$product_loop_id = '';

		$product_loop_classes_arr = apply_filters( 'mc_product_loop_classes', array( 'products' ) );

		$columns = apply_filters( 'loop_shop_columns', 4 );

		if ( defined( 'WC_VERSION' ) && version_compare( WC_VERSION, '3.3', '<' ) ) {
			global $woocommerce_loop;

			$woocommerce_loop['loop'] = 0;

			if( ! empty( $woocommerce_loop['columns'] ) ) {
				$columns = $woocommerce_loop['columns'];
			}

			if( isset( $woocommerce_loop[ 'is_carousel'] ) && $woocommerce_loop[ 'is_carousel' ] ) {
				$product_loop_classes_arr[] = 'products-carousel-' . $columns;
			}

			if( isset( $woocommerce_loop[ 'carousel_id' ] ) && ! empty( $woocommerce_loop[ 'carousel_id' ] ) ) {
				$product_loop_id = 'id="' . esc_attr( $woocommerce_loop[ 'carousel_id' ] ) . '"';
			}
		} else {
			wc_set_loop_prop( 'loop', 0 );

			$new_col = wc_get_loop_prop( 'columns', $columns );
			if( ! empty( $new_col ) ) {
				$columns = $new_col;
			}

			$is_carousel = wc_get_loop_prop( 'is_carousel', '' );
			if( ! empty( $is_carousel ) && $is_carousel ) {
				$product_loop_classes_arr[] = 'products-carousel-' . $columns;
			}

			$carousel_id = wc_get_loop_prop( 'carousel_id', '' );
			if( ! empty( $carousel_id ) ) {
				$product_loop_id = 'id="' . esc_attr( $carousel_id ) . '"';
			}
		}

		$product_loop_classes_arr[] = 'columns-' . $columns;

		$enable_hover = apply_filters( 'mc_product_loop_enable_hover', true );

		if( $enable_hover ) {
			$product_loop_classes_arr[] = 'enable-hover';
		}

		if( is_array( $product_loop_classes_arr ) ) {
			$loop_classes = implode( ' ', $product_loop_classes_arr );
		}

		ob_start();
		?><ul <?php echo $product_loop_id; ?> class="<?php echo esc_attr( $loop_classes ); ?>"><?php
		$loop_start = apply_filters( 'woocommerce_product_loop_start', ob_get_clean() );
		
		if ( $echo ) {
			echo $loop_start; // WPCS: XSS ok.
		} else {
			return $loop_start;
		}
	}
}

if( ! function_exists( 'mc_output_loop_wrapper' ) ) {
	/**
	 * Outputs woocommerce wrapper div start for products
	 *
	 * @return void
	 */
	function mc_output_loop_wrapper() {
		if ( ! woocommerce_products_will_display() ) {
			return;
		}
		if ( defined( 'WC_VERSION' ) && version_compare( WC_VERSION, '3.3', '>=' ) ) {
			if( wc_get_loop_prop( 'is_shortcode' ) ) {
				return;
			}
		}

		$loop_wrapper_classes_arr = array( 'woocommerce' );

		$active_view = apply_filters( 'mc_default_view_switcher_view', 'grid' );
		if( $active_view == 'grid' ) {
			$loop_wrapper_classes_arr[] = 'active';
		}

		$loop_wrapper_classes_arr = apply_filters( 'mc_loop_wrapper_classes', $loop_wrapper_classes_arr );

		$loop_wrapper_classes = implode( ' ', $loop_wrapper_classes_arr );

		?>
		<div id="grid-view" class="<?php echo esc_attr( $loop_wrapper_classes ); ?>">
		<?php
	}
}

if( ! function_exists( 'mc_output_loop_wrapper_end' ) ) {
	/**
	 * End of .woocommerce wrapper div
	 *
	 * @return void
	 */
	function mc_output_loop_wrapper_end() {
		if ( ! woocommerce_products_will_display() ) {
			return;
		}
		if ( defined( 'WC_VERSION' ) && version_compare( WC_VERSION, '3.3', '>=' ) ) {
			if( wc_get_loop_prop( 'is_shortcode' ) ) {
				return;
			}
		}
		?>
		</div><!-- /.woocommerce -->
		<?php
	}
}

if ( ! function_exists( 'mc_product_accessories_tab' ) ) {
	function mc_product_accessories_tab() {
		mc_get_template( 'shop/single-product/tabs/accessories.php' );
	}
}

if( ! function_exists( 'mc_wc_show_product_thumbnails' ) ) {
	function mc_wc_show_product_thumbnails() {
		global $post, $product;
		$columns           = apply_filters( 'woocommerce_product_thumbnails_columns', 4 );
		$post_thumbnail_id = get_post_thumbnail_id( $post->ID );
		$attachment_ids    = $product->get_gallery_image_ids();
		$full_size_image   = wp_get_attachment_image_src( $post_thumbnail_id, 'full' );
		$thumbnail_post    = get_post( $post_thumbnail_id );
		$image_title       = $thumbnail_post->post_content;
		$placeholder       = has_post_thumbnail() ? 'with-images' : 'without-images';
		$wrapper_id        = 'mc-wc-product-gallery-' . uniqid();
		$wrapper_classes   = apply_filters( 'mc_wc_single_product_image_gallery_classes', array(
			'mc-wc-product-gallery',
			'mc-wc-product-gallery--' . $placeholder,
			'mc-wc-product-gallery--columns-' . absint( $columns ),
			'images',
		) );
		$carousel_args     = apply_filters( 'mc_wc_product_thumbnails_carousel_args', array(
			'selector'		=> '.mc-wc-product-gallery__wrapper > .mc-wc-product-gallery__image',
			'animation'		=> 'slide',
			'controlNav'	=> true,
			'animationLoop'	=> false,
			'directionNav'  => false,
			'slideshow'		=> false,
			'itemWidth'		=> 93,
			'itemMargin'	=> 6,
			'asNavFor'		=> '.woocommerce-product-gallery'
		) );
		?>
		<div id="<?php echo esc_attr( $wrapper_id ); ?>" class="<?php echo esc_attr( implode( ' ', array_map( 'sanitize_html_class', $wrapper_classes ) ) ); ?>" data-columns="<?php echo esc_attr( $columns ); ?>">
			<figure class="mc-wc-product-gallery__wrapper">
				<?php
				$attributes = array(
					'title'                   => $image_title,
					'data-large-image'        => $full_size_image[0],
					'data-large-image-width'  => $full_size_image[1],
					'data-large-image-height' => $full_size_image[2],
				);

				if ( has_post_thumbnail() ) {
					$html  = '<figure data-thumb="' . get_the_post_thumbnail_url( $post->ID, 'shop_thumbnail' ) . '" class="mc-wc-product-gallery__image"><a href="' . esc_url( $full_size_image[0] ) . '">';
					$html .= get_the_post_thumbnail( $post->ID, 'shop_thumbnail', $attributes );
					$html .= '</a></figure>';
				} else {
					$html  = '<figure class="mc-wc-product-gallery__image--placeholder">';
					$html .= sprintf( '<img src="%s" alt="%s" class="wp-post-image" />', esc_url( wc_placeholder_img_src() ), esc_html__( 'Awaiting product image', 'mediacenter' ) );
					$html .= '</figure>';
				}

				echo apply_filters( 'mc_wc_single_product_image_thumbnail_html', $html, get_post_thumbnail_id( $post->ID ) );

				if ( $attachment_ids ) {
					foreach ( $attachment_ids as $attachment_id ) {
						$full_size_image  = wp_get_attachment_image_src( $attachment_id, 'full' );
						$thumbnail        = wp_get_attachment_image_src( $attachment_id, 'shop_thumbnail' );
						$thumbnail_post   = get_post( $attachment_id );
						$image_title      = $thumbnail_post->post_content;

						$attributes = array(
							'title'                   => $image_title,
							'data-large-image'        => $full_size_image[0],
							'data-large-image-width'  => $full_size_image[1],
							'data-large-image-height' => $full_size_image[2],
						);

						$html  = '<figure data-thumb="' . esc_url( $thumbnail[0] ) . '" class="mc-wc-product-gallery__image"><a href="' . esc_url( $full_size_image[0] ) . '">';
						$html .= wp_get_attachment_image( $attachment_id, 'shop_thumbnail', false, $attributes );
				 		$html .= '</a></figure>';

						echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html, $attachment_id );
					}
				}
				?>
			</figure>
		</div>
		<?php
		$custom_script = "
			jQuery(document).ready( function($){
				var flex = $( '#" . esc_attr( $wrapper_id ) . "' );
				var flex_args = " . json_encode( $carousel_args ) . ";
				flex_args.asNavFor = flex.siblings( flex_args.asNavFor );
				flex.flexslider( flex_args );
			} );
		";
		wp_add_inline_script( 'media_center-theme-scripts', $custom_script );
	}
}

if ( ! function_exists( 'mc_show_product_images' ) ) {
	function mc_show_product_images() {
		woocommerce_show_product_images();
		if( apply_filters( 'mc_wc_show_product_thumbnails_carousel', false ) ) {
			mc_wc_show_product_thumbnails();
		}
	}
}

if ( ! function_exists( 'mc_wc_single_product_carousel_option_remove_thumb' ) ) {
	function mc_wc_single_product_carousel_option_remove_thumb( $args ) {
		if( apply_filters( 'mc_wc_show_product_thumbnails_carousel', false ) ) {
			$args['controlNav'] = false;
		}
		return $args;
	}
}