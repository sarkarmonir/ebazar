<?php

add_filter( 'pt-ocdi/import_files',      'mc_ocdi_import_files' );
add_action( 'pt-ocdi/after_import',      'mc_ocdi_after_import_setup' );
add_filter( 'pt-ocdi/plugin_intro_text', 'mc_ocdi_modify_intro_text' );