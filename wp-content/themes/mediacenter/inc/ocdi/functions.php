<?php

function mc_ocdi_import_files() {
    return array(
        array(
            'import_file_name'             => 'MediaCenter',
            'categories'                   => array( 'Electronics' ),
            'local_import_file'            => trailingslashit( get_template_directory() ) . 'assets/dummy-data/dummy-data.xml',
            'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'assets/dummy-data/widgets.wie',
            'local_import_redux'           => array(
                array(
                    'file_path'   => trailingslashit( get_template_directory() ) . 'assets/dummy-data/redux-options.json',
                    'option_name' => 'media_center_theme_options',
                ),
            ),
            'import_preview_image_url'     => '//transvelo.github.io/mediacenter/assets/images/preview/mediacenter-preview.png',
            'import_notice'                => esc_html__( 'After you import this demo, you will have to setup the slider separately. Import process may take 15-20 minutes.', 'mediacenter' ),
            'preview_url'                  => 'https://demo2.chethemes.com/mediacenter/',
        ),
    );
}

function mc_ocdi_after_import_setup( $selected_import ) {
    
    // Assign menus to their locations.
    $departments_menu       = get_term_by( 'name', 'All Departments Menu', 'nav_menu' );
    $primary_menu           = get_term_by( 'name', 'Main Menu', 'nav_menu' );
    $topbar_left_menu       = get_term_by( 'name', 'Top Bar Left', 'nav_menu' );
    $topbar_right_menu      = get_term_by( 'name', 'Top Bar Right', 'nav_menu' );
    $handheld_menu          = get_term_by( 'name', 'Vertical Menu', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'departments'  => $departments_menu->term_id,
            'primary'      => $primary_menu->term_id,
            'top-left'     => $topbar_left_menu->term_id,
            'top-right'    => $topbar_right_menu->term_id,
            'hand-held'    => $handheld_menu->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home v1' );
    $blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    update_option( 'page_for_posts', $blog_page_id->ID );

}


function mc_ocdi_modify_intro_text( $plugin_intro_text ) {
    ob_start(); ?>
    <div class="ocdi__intro-notice  notice  notice-warning  is-dismissible">
        <p><?php esc_html_e( 'Before you begin, make sure all the required plugins are activated.', 'mediacenter' ); ?></p>
    </div>

    <div class="ocdi__intro-text">
        <p class="about-description">
            <?php esc_html_e( 'Importing demo data (post, pages, images, theme settings, ...) is the easiest way to setup your theme.', 'mediacenter' ); ?>
            <?php esc_html_e( 'It will allow you to quickly edit everything instead of creating content from scratch.', 'mediacenter' ); ?>
        </p>

        <hr>

        <p><?php esc_html_e( 'When you import the data, the following things might happen:', 'mediacenter' ); ?></p>

        <ul>
            <li><?php esc_html_e( 'No existing posts, pages, categories, images, custom post types or any other data will be deleted or modified.', 'mediacenter' ); ?></li>
            <li><?php esc_html_e( 'Posts, pages, images, widgets, menus and other theme settings will get imported.', 'mediacenter' ); ?></li>
            <li><?php printf( esc_html__( 'Please click on the Import button only once and wait, %s it can take upto 20 minutes %s depending on your server configuration and bandwidth.', 'mediacenter' ),
            '<strong>', '</strong>' ); ?></li>
            <li><?php printf( esc_html__( 'If you face any issues while importing, please do not worry, you can reach our %s theme support %s and we\'ll help you resolve the issue as soon as possible', 'mediacenter' ),
            '<a href="https://madrasthemes.freshdesk.com/" target="_blank"><strong>', '</strong></a>' ); ?></li>
        </ul>

        <hr>
    </div>
    <?php $plugin_intro_text = ob_get_clean();
    return $plugin_intro_text;
}